variable "private_key_path" {
  type = string
}

variable "repository_url" {
  type = string
}

resource "aws_instance" "example" {
  ami           = "ami-0c94855ba95c71c99"
  instance_type = "t2.micro"
  key_name      = "my-key"
  security_groups = ["default"]

  provisioner "file" {
    source      = var.private_key_path
    destination = "/tmp/private_key"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y git",
      "eval $(ssh-agent -s)",
      "ssh-add /tmp/private_key",
      "ssh-keyscan github.com >> ~/.ssh/known_hosts",
      "git clone ${var.repository_url}"
    ]
  }

  tags = {
    Name = "example-instance"
  }
}
