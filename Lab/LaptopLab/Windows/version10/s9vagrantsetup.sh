mkdir ~/vagrant
mkdir -p ~/vagrant/centos ~/vagrant/ubuntu ~/vagrant/bento
cd ~/vagrant/centos
vagrant init geerlingguy/centos8
vagrant up && vagrant halt
cd ~/vagrant/bento
vagrant init bento/ubuntu-20.04
vagrant up && vagrant halt
cd ~/vagrant/ubuntu
vagrant init ubuntu/xenial64
vagrant up && vagrant halt
