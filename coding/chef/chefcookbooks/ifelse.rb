if node['platform'] == 'ubuntu'
  package 'apache2' do
    action :install
  end
elsif node['platform'] == 'centos'
  package 'httpd' do
    action :install
  end
end
